from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import os
from dotenv import load_dotenv

load_dotenv()


def set_up():
    """Sets up configuration for the app"""

    env = os.getenv("ENV", ".env")
    config = {
        "DB_USER": os.getenv("DB_USER"),
        "DB_PASSWORD": os.getenv("DB_PASSWORD"),
        "DB_URL": os.getenv("DB_URL"),
        "DB_NAME": os.getenv("DB_NAME"),
    }
    return config


config = set_up()
engine = create_engine(
    f"postgresql+psycopg2://{config['DB_USER']}:{config['DB_PASSWORD']}@{config['DB_URL']}:5432/{config['DB_NAME']}")
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()
