from pydantic import BaseModel


class Dish(BaseModel):
    id: int
    name: str
    type: int
    description: str

    class Config:
        orm_mode = True
