from sqlalchemy.orm import Session
from sqlalchemy.sql import func
from db import models, schemas


def get_dish(db: Session, dish_id: int):
    return db.query(models.Dish).filter(models.Dish.id == dish_id).first()


def get_dishes(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.Dish).offset(skip).limit(limit).all()


def create_dish(db: Session, dish: schemas.Dish):
    db_dish = models.Dish(id=dish.id, name=dish.name, type=dish.type, description=dish.description)
    db.add(db_dish)
    db.commit()
    db.refresh(db_dish)
    return db_dish


def delete_dish(db: Session, dish_id: int):
    db.query(models.Dish).filter(models.Dish.id == dish_id).delete()
    db.commit()
    return True


def get_random_dish(db: Session):
    return db.query(models.Dish).order_by(func.random()).first()
