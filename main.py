from fastapi import FastAPI
from api.api import api_router

app = FastAPI(
    title="Dbaret Ilyoum - Backend",
    description="Tunisian Dish Suggesstion for Ramadhan",
    version="1.0.0",
)
app.include_router(api_router, prefix="/api")
