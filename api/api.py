from fastapi import APIRouter
from api.endpoints import dish

api_router = APIRouter()

api_router.include_router(dish.router, prefix="/dish", tags=["dish"])


@api_router.get("/")
async def root():
    return {"message": "Achba7 - API"}
