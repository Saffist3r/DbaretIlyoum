from typing import List

from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer

from sqlalchemy.orm import Session
from starlette.responses import Response
from starlette.status import HTTP_204_NO_CONTENT

from db import models, schemas
from db.crud import dish
from db.database import SessionLocal, engine

models.Base.metadata.create_all(bind=engine)

router = APIRouter()
token_auth_scheme = OAuth2PasswordBearer(tokenUrl="token")


# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@router.post("/")
async def create_dish(dish_data: schemas.Dish, db: Session = Depends(get_db)):
    db_user = models.Dish(name=dish_data.name, description=dish_data.description, type=dish_data.type)
    resp = dish.create_dish(db, db_user)
    return resp


@router.get("/dishes/", response_model=List[schemas.Dish])
async def get_dishes(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    dishes = dish.get_dishes(db, skip=skip, limit=limit)
    return dishes


@router.get("/dishes/random", response_model=schemas.Dish)
async def get_random_dish(db: Session = Depends(get_db)):
    dishes = dish.get_random_dish(db)
    return dishes


@router.get("/{dish_id}", response_model=schemas.Dish)
async def get_dishes_by_id(dish_id: int, db: Session = Depends(get_db)):
    db_user = dish.get_dish(db, dish_id=dish_id)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_user


@router.delete("/{dishes_id}", responses={204: {"model": None}})
async def delete_dish_by_id(dish_id: int, db: Session = Depends(get_db)):
    db_dish = dish.get_dish(db, dish_id=dish_id)
    if db_dish is None:
        raise HTTPException(status_code=404, detail="User not found")
    else:
        dish.delete_dish(db, dish_id)
    return Response(status_code=HTTP_204_NO_CONTENT)
